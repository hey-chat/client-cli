import getopt
import json
import logging
import ssl
import sys
import threading
import time

import websocket

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

isConnected = False
chat_input_thread = None


def get_userid():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "u:", ["user"])
        if not opts:
            log.error('user id not provided')
            sys_exit_incorrect_args()
        for opt, arg in opts:
            if opt in ("-u", "-user") and arg:
                return arg
            log.error('wrong argument provided')
            sys_exit_incorrect_args()
    except getopt.GetoptError:
        sys_exit_incorrect_args()


def sys_exit_incorrect_args():
    log.info('{}'.format('main.py -u <userid>'))
    sys.exit(1)


def on_message(ws, message):
    log.debug(message)
    if isinstance(message, str):
        message = json.loads(message)
    if 'userId' in message and 'sentTime' not in message:
        print('incomming message: {} format error.'.format(message))
        return
    print('{}_{}: {}'.format(message['userId'], message['sentTime'], message['message']))


def on_error(ws, error):
    print(error)


def on_open(ws):
    print('websocket connection opened')


def on_close(ws):
    if chat_input_thread and chat_input_thread.is_alive():
        chat_input_thread.join()
    print("### closed ###")


class ChatInputThread(threading.Thread):

    def __init__(self, ws, user):
        threading.Thread.__init__(self)
        self.ws = ws
        self.userId = user
        self.run_flag = True

    def run(self):
        log.info('\n*****Chat Area*****')
        while True and run_flag:
            try:
                time.sleep(1)
                message_input = input()
                message_json = {
                    'action': 'messageReceive',
                    'message': message_input
                }
                if not run_flag:
                    break
                self.ws.send(json.dumps(message_json))
                log.info('message sent')
            except Exception:
                print('Good Bye: {}'.format(self.userId))
                ws.close()
                break

    def kill(self):
        self.run_flag = False

if __name__ == "__main__":
    userId = get_userid()
    url = 'wss://ya3yz1me9g.execute-api.eu-west-1.amazonaws.com/dev?userid={}'.format(userId)
    log.debug('establishing a websocket connection to the heychat backend')
    websocket.enableTrace(False)
    ws = websocket.WebSocketApp(url,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open
    run_flag = True
    chat_input_thread = ChatInputThread(ws, userId)
    try:
        chat_input_thread.start()
        ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE, "check_hostname": False})
    except Exception as e:
        log.error('error: {}, occurred thus aborting chat app!, see you again '.format(e))
        log.info('Good Bye: {}'.format(userId))
        if chat_input_thread.is_alive():
            chat_input_thread.kill()
            chat_input_thread.join(1)
        sys.exit(0)
    except KeyboardInterrupt:
        log.info('Good Bye: {}'.format(userId))
        if chat_input_thread.is_alive():
            chat_input_thread.kill()
            chat_input_thread.join(1)
        sys.exit(0)
